import Head from 'next/head'

const Meta = ({title, keywords, description}) => {
  return (
    <Head>
        <meta name="viewport" content="width=device-width, initial-1"/>
        <meta name='keywords' content={keywords} />
        <meta name="description" content={description} />
        <meta charset="utf-8" />
        <link rel='icon' href='/favicon.ico' />
        <title>{title}</title>
    </Head>
  )
}

Meta.defaultProps = {
    title: 'WbDev news',
    keywords: 'web development, programming',
    description: 'Get the latest webdev news'
}

export default Meta