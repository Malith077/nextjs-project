import ArticleItem from './ArticleItem'
import articeStyles from '../styles/Article.module.css'

const ArticleList = ({articles}) => {
  return (
    <div className={articeStyles.grid}>
        {articles.map((article) => (<ArticleItem article={article} key={article.id.toString()} />))}
    </div>
  )
}


export default ArticleList